/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Hades
 */
public class SinhVien {
    String MSSV;
    String hoTen;
    String ngaySinh;
    double diemToan;
    double diemLy;
    double diemHoa;
    double diemTB;

    public SinhVien() {
    }

    public SinhVien(String MSSV, String hoTen, String ngaySinh, double diemToan, double diemLy, double diemHoa) {
        this.MSSV = MSSV;
        this.hoTen = hoTen;
        this.ngaySinh = ngaySinh;
        this.diemToan = diemToan;
        this.diemLy = diemLy;
        this.diemHoa = diemHoa;
        this.diemTB = (double)((diemToan+diemLy+diemHoa)/3);
    }

    public SinhVien(String MSSV, String hoTen, String ngaySinh, double diemToan, double diemLy, double diemHoa, double diemTB) {
        this.MSSV = MSSV;
        this.hoTen = hoTen;
        this.ngaySinh = ngaySinh;
        this.diemToan = diemToan;
        this.diemLy = diemLy;
        this.diemHoa = diemHoa;
        this.diemTB = diemTB;
    }

    public double getDiemHoa() {
        return diemHoa;
    }

    public double getDiemLy() {
        return diemLy;
    }

    public double getDiemTB() {
        return diemTB;
    }

    public double getDiemToan() {
        return diemToan;
    }

    public String getHoTen() {
        return hoTen;
    }

    public String getMSSV() {
        return MSSV;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setDiemHoa(double diemHoa) {
        this.diemHoa = diemHoa;
    }

    public void setDiemLy(double diemLy) {
        this.diemLy = diemLy;
    }

    public void setDiemTB(double diemTB) {
        this.diemTB = diemTB;
    }

    public void setDiemToan(double diemToan) {
        this.diemToan = diemToan;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public void setMSSV(String MSSV) {
        this.MSSV = MSSV;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }
    
}
